#pragma once
#include "mapper2d.h"



namespace srrg_mapper2d {

  class Mapper2DParams {
  public:
    Mapper2DParams(Mapper2D* mapper_ = NULL);
    
    void applyParams();

    inline void setMapper(Mapper2D* mapper_) {_mapper = mapper_;}
    inline void setVerbose(bool verbose_) {_verbose = verbose_;}
    inline bool verbose() {return _verbose;}
    inline void setLogTimes(bool log_times_) {_log_times = log_times_;}
    inline bool logTimes() {return _log_times;}
    inline void setClosuresInlierThreshold(double closures_inlier_threshold_) {_closures_inlier_threshold = closures_inlier_threshold_;}
    inline double closuresInlierThreshold() {return _closures_inlier_threshold;}
    inline void setClosuresWindow(int closures_window_) {_closures_window = closures_window_;}
    inline int closuresWindow() {return _closures_window;}
    inline void setClosuresMinInliers(int closures_min_inliers_) {_closures_min_inliers = closures_min_inliers_;}
    inline int closuresMinInliers() {return _closures_min_inliers;}
    inline void setLcalignerInliersDistance(float lcaligner_inliers_distance_) {_lcaligner_inliers_distance = lcaligner_inliers_distance_;}
    inline float lcalignerInliersDistance() {return _lcaligner_inliers_distance;}
    inline void setLcalignerMinInliersRatio(float lcaligner_min_inliers_ratio_) {_lcaligner_min_inliers_ratio = lcaligner_min_inliers_ratio_;}
    inline float lcalignerMinInliersRatio() {return _lcaligner_min_inliers_ratio;}
    inline void setLcalignerMinNumCorrespondences(int lcaligner_min_num_correspondences_) {_lcaligner_min_num_correspondences = lcaligner_min_num_correspondences_;}
    inline int lcalignerMinNumCorrespondences() {return _lcaligner_min_num_correspondences;}
    inline void setNumMatchingBeams(int num_matching_beams_) {_num_matching_beams = num_matching_beams_;}
    inline int numMatchingBeams() {return _num_matching_beams;}
    inline void setMatchingFov(double matching_fov_) {_matching_fov = matching_fov_;}
    inline double matchingFov() {return _matching_fov;}
    
    inline void setUseMerger(bool use_merger_) {_use_merger = use_merger_;}
    inline bool useMerger() {return _use_merger;}
    
    inline void setVertexTranslationThreshold(double vertex_translation_threshold_) {_vertex_translation_threshold = vertex_translation_threshold_;}
    inline double vertexTranslationThreshold() {return _vertex_translation_threshold;}
    inline void setVertexRotationThreshold(double vertex_rotation_threshold_) {_vertex_rotation_threshold = vertex_rotation_threshold_;}
    inline double vertexRotationThreshold() {return _vertex_rotation_threshold;}

  protected:
    
    Mapper2D* _mapper;

    //GRAPH parameters
    double _closures_inlier_threshold;
    int _closures_window;
    int _closures_min_inliers;
    float _lcaligner_inliers_distance;
    float _lcaligner_min_inliers_ratio;
    int _lcaligner_min_num_correspondences;
    int _num_matching_beams;
    double _matching_fov;

    //MAPPER PARAMETERS
    bool _verbose;
    bool _log_times;
    bool _use_merger;
    double _vertex_translation_threshold;
    double _vertex_rotation_threshold;

  };

}
