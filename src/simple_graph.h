#pragma once
// g2o
#include "g2o/core/optimizable_graph.h"
#include "g2o/core/sparse_optimizer.h"
#include "g2o/core/block_solver.h"
#include "g2o/core/factory.h"
#include "g2o/core/optimization_algorithm_factory.h"
#include "g2o/core/optimization_algorithm_gauss_newton.h"
#include "g2o/core/optimization_algorithm_levenberg.h"
#include "g2o/solvers/csparse/linear_solver_csparse.h"
#include "g2o/types/slam2d/vertex_se2.h"
#include "g2o/types/slam2d/edge_se2.h"
#include "g2o/types/data/vertex_ellipse.h"
#include "g2o/types/data/robot_laser.h"

//scan_matcher
#include "cloud2d.h"
#include "cloud2d_trajectory.h"
#include "cloud2d_aligner.h"

#include "vertices_finder.h"
#include "closure_buffer.h"
#include "closure_checker.h"

namespace srrg_mapper2d{

using namespace g2o;
using namespace std;
using namespace Eigen;
using namespace srrg_core;
using namespace srrg_scan_matcher;

typedef std::map<VertexSE2*, Cloud2DWithTrajectory*> VertexSE2Cloud2DMap;
typedef BlockSolver< BlockSolverTraits<-1, -1> >  SlamBlockSolver;
typedef LinearSolverCSparse<SlamBlockSolver::PoseMatrixType> SlamLinearSolver;

/////////////////
//Helper struct
struct DiscreteTriplet {
    DiscreteTriplet(Eigen::Vector3f& tr, float dx, float dy, float dth) {
        ix=(int)(tr.x()/dx);
        iy=(int)(tr.y()/dy);
        ith = (int)(tr.z()/dth);
    }

    bool operator < (const DiscreteTriplet& dt) const {
        if (ix<dt.ix )
            return true;
        if (ix == dt.ix && iy<dt.iy)
            return true;
        if (ix == dt.ix && iy == dt.iy && ith < dt.ith)
            return true;
        return false;
    }
    float ix, iy, ith;
};
/////////////////

class SimpleGraph{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    SimpleGraph();
    void addVertex(Eigen::Vector3f& pose, Cloud2DWithTrajectory* cloud);
    EdgeSE2* createEdge(VertexSE2* vfrom, VertexSE2* vto, SE2& relative_transf, Eigen::Matrix3d& inf);
    void addEdge(VertexSE2* vfrom, VertexSE2* vto, SE2& relative_transf, Eigen::Matrix3d& inf);
    void matchVertex(VertexSE2* v);
    bool tryAlign(Eigen::Isometry2f& initial_guess);
    bool tryAlign(VertexSE2* v_ref, VertexSE2* v_cur, const Eigen::Isometry2f& initial_guess = Eigen::Isometry2f::Identity());

    SE2 getSE2TransfFromAligner();
    void createCandidateEdgeFromAligner(VertexSE2* vfrom, VertexSE2* vto, std::map<DiscreteTriplet, EdgeSE2*>& candidateEdges);

    void optimize(int nrunnings);

    inline VertexSE2* currentVertex() {return _currentVertex;}
    inline void setCurrentVertex(VertexSE2* currentVertex_) {_currentVertex = currentVertex_;}
    inline bool isFirstVertex() {return _isFirstVertex;}
    inline SparseOptimizer* graph(){return _graph;}
    Cloud2DWithTrajectory* vertexCloud(VertexSE2* v);
    bool saveGraph(const char *filename);
    void saveClouds(const char *prefix = 0);

    void addScanFromCloud(VertexSE2* v);
    RobotLaser* computeScanFromCloud(VertexSE2* v, const Eigen::Isometry2f& transform=Eigen::Isometry2f::Identity());
    RobotLaser* computeScanFromWaypoint(VertexSE2* v, srrg_core::LaserMessage* laser, const Eigen::Isometry2f& transform);
    RobotLaser* computeScanFromCloudWaypoint(VertexSE2* v, const Eigen::Isometry2f& transform, size_t wp);

    void checkClosures();

    void addEllipseData(OptimizableGraph::Vertex* v);
    VertexEllipse* findEllipseData(OptimizableGraph::Vertex* v);
    void updateEllipseData();

    RobotLaser* findLaserData(OptimizableGraph::Vertex* v);

    //Parameters for Loop Closure Verification
    inline void setLoopClosureInlierThreshold(float loopClosureInlierThreshold_){_loopClosureInlierThreshold = loopClosureInlierThreshold_;}
    inline void setLoopClosureWindow(int loopClosureWindow_){_loopClosureWindow = loopClosureWindow_;}
    inline void setLoopClosureMinInliers(int loopClosureMinInliers_){_loopClosureMinInliers = loopClosureMinInliers_;}

    //Parameters for alignment verification
    inline void setInliersDistance(float inliersDistance_){_c2DAligner->setInliersDistance(inliersDistance_);}
    inline void setMinInliersRatio(float minInliersRatio_){_c2DAligner->setMinInliersRatio(minInliersRatio_);}
    inline void setMinNumCorrespondences(int minNumCorrespondences_){_c2DAligner->setMinNumCorrespondences(minNumCorrespondences_);}

    inline void setGraph(SparseOptimizer * graph){_graph = graph;}
    inline void setVerticesClouds(VertexSE2Cloud2DMap& verticesClouds){_verticesClouds = verticesClouds;}
    inline VertexSE2Cloud2DMap& verticesClouds(){return _verticesClouds;}
    inline ClosureBuffer& closureBuffer(){return _candidate_closures;}

    void removeVertex(VertexSE2* v);

    inline std::set<OptimizableGraph::VertexSet>& verticesToMerge(){return _vertices_to_merge;}

    void exportTrajectory(const char *filename);
    void exportStampedTrajectory(const char *filename);
    void exportTrajectoryOriginalLaser(const char *filename);

    void loadGraph(const char *filename);

    inline void setNumRanges(int num_ranges_){_num_ranges = num_ranges_;}
    inline void setFov(float fov_){_fov = fov_;}

    void clear();
    
protected:
    SparseOptimizer* _graph;
    Cloud2DAligner *_c2DAligner;
    Projector2D* _projector;
    int _currentId;
    int _currentSessionId;
    VertexSE2* _currentVertex;
    bool _isFirstVertex;
    VertexSE2Cloud2DMap _verticesClouds;
    ClosureBuffer _candidate_closures;

    int _loopClosureWindow;
    float _loopClosureInlierThreshold;
    int _loopClosureMinInliers;

    int _num_ranges;
    float _fov;

    float delta_closure_translation_cropping_threshold;

    std::set<OptimizableGraph::VertexSet> _vertices_to_merge;
};

}
